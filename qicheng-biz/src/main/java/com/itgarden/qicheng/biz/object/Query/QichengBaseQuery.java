package com.itgarden.qicheng.biz.object.Query;

public class QichengBaseQuery {
	// 查询参数
	private Integer pageSize;
	private Integer pageIndex;
	private Integer startPos;
	private Integer endPos;
	// 结果参数
	private Integer totalPage;
	private Integer totalCount;
	private static final Integer defaultPageSize = 3;
	private static final Integer defaultPageIndex = 1;

	public Integer getPageSize() {
		if (pageSize == null) {
			pageSize = defaultPageSize;
		}
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public Integer getPageIndex() {
		return pageIndex;
	}

	public void setPageIndex(Integer pageIndex) {
		if (pageIndex == null) {
			pageIndex = defaultPageIndex;
		}
		this.pageIndex = pageIndex;
	}

	public Integer getStartPos() {
		startPos = (pageIndex - 1) * pageSize;
		return startPos;
	}

	public void setStartPos(Integer startPos) {
		this.startPos = startPos;
	}

	public Integer getEndPos() {
		endPos = pageIndex * pageSize;
		return endPos;
	}

	public void setEndPos(Integer endPos) {
		this.endPos = endPos;
	}

	public Integer getTotalPage() {
		totalPage = (totalCount % pageSize) == 0 ? totalCount / pageSize
				: totalCount / pageSize + 1;
		return totalPage;
	}

	public void setTotalPage(Integer totalPage) {
		this.totalPage = totalPage;
	}

	public Integer getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}

}
