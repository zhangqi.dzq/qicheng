package com.itgarden.qicheng.biz.object.Query;

import java.util.List;

import com.itgarden.qicheng.biz.object.entity.QichengArticleDO;

public class QichengArticleQuery extends QichengBaseQuery {
	private List<QichengArticleDO> result;

	public List<QichengArticleDO> getResult() {
		return result;
	}

	public void setResult(List<QichengArticleDO> result) {
		this.result = result;
	}

}
