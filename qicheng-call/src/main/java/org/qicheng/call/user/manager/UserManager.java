package org.qicheng.call.user.manager;

import javax.annotation.Resource;

import org.qcservice.client.usercenter.UserOpenService;
import org.qcservice.common.entity.Result;
import org.qcservice.common.entity.UserDO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UserManager {
	private static final Logger logger = LoggerFactory
			.getLogger(UserManager.class);

	private UserOpenService userOpenService;
	/**
	 * 注册
	 * 
	 * @param user
	 * @return
	 */
	public Result<UserDO> register(UserDO user) {
		Result<UserDO> result = new Result<UserDO>();
		try {
			result = userOpenService.write(user);
		} catch (Exception e) {
			logger.error("注册失败，原因：" + e);
			result.setErrorMsg("注册失败！");
			result.setSucess(Boolean.FALSE);
			return result;
		}
		return result;
	}

	/**
	 * 登陆
	 * 
	 * @param user
	 * @return
	 */
	public Result<UserDO> login(UserDO user) {
		Result<UserDO> result = new Result<UserDO>();
		try {
			System.out.print(userOpenService);
			return userOpenService.query(user);
		} catch (Exception e) {
			result.setSucess(Boolean.FALSE);
			result.setErrorMsg("登陆失败");
			return result;
		}
	}
	/**
	 * 查询
	 * @param user
	 * @return
	 */
	public Result<UserDO> query(UserDO user) {
		Result<UserDO> result = new Result<UserDO>();
		try {
			System.out.print(userOpenService);
			return userOpenService.query(user);
		} catch (Exception e) {
			result.setSucess(Boolean.FALSE);
			result.setErrorMsg("查询失败");
			return result;
		}
	}
	

	public UserOpenService getUserOpenService() {
		return userOpenService;
	}

	public void setUserOpenService(UserOpenService userOpenService) {
		this.userOpenService = userOpenService;
	}

}
