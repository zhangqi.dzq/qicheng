package com.itgarden.qicheng.web.module.screen;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * 约定：程序中所有的错误提醒，都要通过转发到ErrorScreen间接返回给error页面
 * 
 * @author zhangqidzq
 *
 */
@Controller
public class ErrorScreen {

	@RequestMapping("screen/error")
	public ModelAndView execute(String result) {
		ModelAndView mav = new ModelAndView("screen/error");
		mav.addObject("result", result);
		return mav;
	}
}
