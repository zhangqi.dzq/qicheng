package com.itgarden.qicheng.web.module.action.article;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.itgarden.qicheng.biz.object.entity.QichengArticleDO;
import com.itgarden.qicheng.service.article.QichengArticleService;

@Controller
public class ArticleAction {

	private static final Logger logger = LoggerFactory
			.getLogger(ArticleAction.class);

	@Resource
	private QichengArticleService qichengArticleService;

	@RequestMapping("action/article/articleAction/doEdit")
	public ModelAndView doEdit(@RequestParam("art_title") String artTitle,
			@RequestParam("art_content") String artContent) {
		logger.info("wellcome to ArticleAction");
		ModelAndView mav = new ModelAndView();
		if (StringUtils.isBlank(artTitle) || StringUtils.isBlank(artContent)) {
			String result = "forward:/screen/error?result=保存失败！原因：博文名称或博文内容没有填写！";
			mav.setViewName(result);
			return mav;
		}
		try {
			QichengArticleDO qichengArticleDO = assemble(artTitle, artContent);
			Long artId = qichengArticleService.addArticle(qichengArticleDO);

			mav.setViewName("forward:/screen/article/detail?artId=" + artId);
			return mav;
		} catch (Exception e) {
			logger.error("保存博文发生异常" + e);
			mav.setViewName("保存博文错误，请联系客服处理！");
			return mav;
		}
	}

	public QichengArticleDO assemble(String articleTitle, String articleContent) {
		QichengArticleDO qichengArticleDO = new QichengArticleDO();
		qichengArticleDO.setArticleTitle(articleTitle);
		qichengArticleDO.setArticleContent(articleContent);
		return qichengArticleDO;
	}
}
