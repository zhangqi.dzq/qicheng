package com.itgarden.qicheng.web.module.screen.article;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class EditScreen {

	@RequestMapping("screen/article/edit")
	public ModelAndView execute() {
		return new ModelAndView("screen/article/edit");
	}
}
