package com.itgarden.qicheng.web.module.action.user;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.qcservice.common.entity.Result;
import org.qcservice.common.entity.UserDO;
import org.qicheng.call.user.manager.UserManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class UserAction {

	@Resource
	private UserManager userManager;

	private static final Logger logger = LoggerFactory
			.getLogger(UserAction.class);

	@RequestMapping("action/user/userAction/doLogin")
	public ModelAndView doLogin(@RequestParam("userName") String userName,
			@RequestParam("passWord") String passWord) {
		logger.info("wellcome to userAction!");
		ModelAndView mav = new ModelAndView();
		String page = null;
		UserDO user = new UserDO();
		user.setUserName(userName);
		user.setPassWord(passWord);
		Result<UserDO> result = userManager.login(user);
		if (result == null || !result.isSucess()) {
			logger.error("登陆失败！result");
			page = "forward:/screen/error?result=登陆失败，用户名或密码错误！！";
		} else {
			page = "forward:/screen/success?result=登陆成功！！！";

		}
		mav.setViewName(page);
		return mav;
	}
	
	@RequestMapping("action/user/userAction/doRegister")
	public ModelAndView doRegister(@RequestParam("userName") String userName,
			@RequestParam("passWord") String passWord){
		
		ModelAndView mav = new ModelAndView();
		String page = null;
		if(StringUtils.isBlank(userName) || StringUtils.isBlank(passWord)){
			page = "forward:/screen/error?result=注册失败，用户名和密码不能为空！！";
			mav.setViewName(page);
			return mav;
		}
		UserDO user = new UserDO();
		user.setUserName(userName);
		Result<UserDO> queryResult = userManager.query(user);
		if(queryResult!= null && queryResult.isSucess()){
			page = "forward:/screen/error?result=用户名已经存在！！";
			mav.setViewName(page);
			return mav;
		}
		user.setPassWord(passWord);
		Result<UserDO> registerResult = userManager.register(user);
		if (registerResult == null || !registerResult.isSucess()) {
			page = "forward:/screen/error?result=注册失败!";
		} else {
			page = "forward:/screen/success?result=注册成功！！！";
		}
		mav.setViewName(page);
		return mav;
	}
	
	
}
