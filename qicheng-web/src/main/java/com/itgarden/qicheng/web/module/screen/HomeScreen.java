package com.itgarden.qicheng.web.module.screen;

import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.itgarden.qicheng.biz.object.Query.QichengArticleQuery;
import com.itgarden.qicheng.biz.object.entity.QichengArticleDO;
import com.itgarden.qicheng.service.article.QichengArticleService;

@Controller
public class HomeScreen {
	@Resource
	private QichengArticleService qichengArticleService;
	private static final Logger logger = LoggerFactory
			.getLogger(HomeScreen.class);

	@RequestMapping("/")
	public ModelAndView doHomePage(Integer curPage) {
		logger.info("wellcome to HomeSceen");

		ModelAndView mav = new ModelAndView("screen/home");
		QichengArticleQuery query = new QichengArticleQuery();
		query.setPageSize(30);
		query.setPageIndex(curPage);
		List<QichengArticleDO> result = qichengArticleService
				.queryByPage(query);
		mav.addObject("curPage", query.getPageIndex());
		mav.addObject("result", result);
		mav.addObject("totalPage", query.getTotalPage());
		return mav;
	}
}
