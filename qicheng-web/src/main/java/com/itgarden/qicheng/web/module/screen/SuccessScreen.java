package com.itgarden.qicheng.web.module.screen;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * 约定：程序中所有的成功提醒，都要通过转发到SuccessScreen间接返回给success页面
 * 
 * @author zhangqidzq
 *
 */
@Controller
public class SuccessScreen {

	@RequestMapping("screen/success")
	public ModelAndView execute(String result) {
		ModelAndView mav = new ModelAndView("screen/success");
		mav.addObject("result", result);
		return mav;
	}
}
