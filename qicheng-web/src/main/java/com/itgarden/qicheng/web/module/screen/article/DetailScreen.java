package com.itgarden.qicheng.web.module.screen.article;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.itgarden.qicheng.biz.object.entity.QichengArticleDO;
import com.itgarden.qicheng.service.article.QichengArticleService;

@Controller
public class DetailScreen {

	private static final Logger logger = LoggerFactory
			.getLogger(DetailScreen.class);

	@Resource
	private QichengArticleService qichengArticleService;

	@RequestMapping("screen/article/detail")
	public ModelAndView doDetail(@RequestParam("artId") Long artId) {
		logger.info("wellcome to DetailScreen");
		ModelAndView mav = new ModelAndView();
		QichengArticleDO qichengArticleDO = qichengArticleService
				.queryById(artId);
		if (qichengArticleDO == null) {
			String result = "温馨提示：您检索的博文不存在！";
			mav.setViewName("forward:/screen/error?result=" + result);
			return mav;
		}
		mav.setViewName("screen/article/detail");
		mav.addObject("artTitle", qichengArticleDO.getArticleTitle());
		mav.addObject("artContent", qichengArticleDO.getArticleContent());
		return mav;
	}
}
