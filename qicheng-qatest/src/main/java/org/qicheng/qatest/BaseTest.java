package org.qicheng.qatest;

import org.qcservice.client.usercenter.UserOpenService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Hello world!
 *
 */
public class BaseTest {
	public static void main(String[] args) {
		@SuppressWarnings("resource")
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("spring/spring-dal.xml","spring/spring-datasource.xml","spring-dubbo-consumer.xml");
		context.start();
		//		UserOpenService userOpenService = (UserOpenService) context.getBean("userOpenService");
//		System.out.print(userOpenService.queryById(1L));
	}
}
