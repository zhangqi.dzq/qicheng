package com.itgarden.qicheng.service.article;

import java.util.List;

import com.itgarden.qicheng.biz.object.Query.QichengArticleQuery;
import com.itgarden.qicheng.biz.object.entity.QichengArticleDO;

public interface QichengArticleService {
	public Long addArticle(QichengArticleDO articleDO);

	public QichengArticleDO queryById(Long id);

	public List<QichengArticleDO> queryByPage(QichengArticleQuery query);

}
