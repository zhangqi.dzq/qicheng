package com.itgarden.qicheng.service.impl.article;

import java.util.List;

import javax.annotation.Resource;

import com.itgarden.qicheng.biz.object.Query.QichengArticleQuery;
import com.itgarden.qicheng.biz.object.entity.QichengArticleDO;
import com.itgarden.qicheng.dao.article.QichengArticleDao;
import com.itgarden.qicheng.service.article.QichengArticleService;

public class QichengArticleServiceImpl implements QichengArticleService {
	// @Resource
	private QichengArticleDao qichengArticleDao;

	@Override
	public Long addArticle(QichengArticleDO articleDO) {

		return qichengArticleDao.addArticle(articleDO);
	}

	@Override
	public QichengArticleDO queryById(Long id) {
		return qichengArticleDao.queryById(id);
	}

	@Override
	public List<QichengArticleDO> queryByPage(QichengArticleQuery query) {
		return qichengArticleDao.queryByQuery(query);
	}

	public QichengArticleDao getQichengArticleDao() {
		return qichengArticleDao;
	}

	public void setQichengArticleDao(QichengArticleDao qichengArticleDao) {
		this.qichengArticleDao = qichengArticleDao;
	}

}
