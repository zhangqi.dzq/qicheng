package com.itgarden.qicheng.dao.article;

import java.util.List;

import com.itgarden.qicheng.biz.object.Query.QichengArticleQuery;
import com.itgarden.qicheng.biz.object.entity.QichengArticleDO;

public interface QichengArticleDao {

	public Long addArticle(QichengArticleDO articleDO);

	public QichengArticleDO queryById(Long id);

	public List<QichengArticleDO> queryByQuery(QichengArticleQuery query);

	public Integer queryCount();
}
