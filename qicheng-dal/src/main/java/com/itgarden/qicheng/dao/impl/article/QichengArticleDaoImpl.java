package com.itgarden.qicheng.dao.impl.article;

import java.util.List;

import com.itgarden.qicheng.biz.object.Query.QichengArticleQuery;
import com.itgarden.qicheng.biz.object.entity.QichengArticleDO;
import com.itgarden.qicheng.dao.QichengBaseDao;
import com.itgarden.qicheng.dao.article.QichengArticleDao;

public class QichengArticleDaoImpl extends QichengBaseDao implements
		QichengArticleDao {

	@SuppressWarnings("deprecation")
	@Override
	public Long addArticle(QichengArticleDO articleDO) {
		return (Long) this.getSqlMapClientTemplate().insert("article.add",
				articleDO);
	}

	@SuppressWarnings("deprecation")
	@Override
	public QichengArticleDO queryById(Long id) {
		return (QichengArticleDO) this.getSqlMapClientTemplate()
				.queryForObject("article.queryById", id);
	}

	@SuppressWarnings({ "deprecation", "unchecked" })
	@Override
	public List<QichengArticleDO> queryByQuery(QichengArticleQuery query) {
		query.setTotalCount(this.queryCount());
		List<QichengArticleDO> result = this.getSqlMapClientTemplate()
				.queryForList("article.queryByPage", query);
		query.setResult(result);
		return result;
	}

	@SuppressWarnings("deprecation")
	@Override
	public Integer queryCount() {
		return (Integer) this.getSqlMapClientTemplate().queryForObject(
				"article.count");
	}
}
